var Solution;

Solution = (function() {

  function Solution(gen) {
    this.gen = gen != null ? gen : 0;
    return;
  }

  return Solution;

})();

exports.Solution = Solution;
